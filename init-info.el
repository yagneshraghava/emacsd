;;; init-info.el
;; Copyright (C) Yagnesh Raghava Yakkala. http://yagnesh.org
;; License: GPL v3 or later

;;; Commentary:
;; info , searching docs, short cuts

(require 'info-look)
(setq Info-additional-directory-list Info-default-directory-list)

;;; init-info.el ends here
