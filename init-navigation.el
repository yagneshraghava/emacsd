;;; init-navigation.el
;;
;;    File: init-navigation.el
;; Created: Tuesday, December 13 2011

;;; Description:
;; navigation with in buffer related setup

(require 'ace-jump-mode)
(define-key global-map
  (kbd "M-l") 'ace-jump-mode)

;;; init-navigation.el ends here
