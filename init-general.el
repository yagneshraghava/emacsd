;;; init-general.el
;; Copyright (C) Yagnesh Raghava Yakkala. http://yagnesh.org
;; License: GPL v3 or later

;;; General settings

;;; Some environment variables
(setenv "LANG" "C")
(setenv "LC_COLLATE" "en_US.UTF-8")
(setenv "LC_CTYPE" "en_US.UTF-8")
(setenv "LC_MESSAGES" "en_US.UTF-8")

(setenv "PAGER" "cat")
(setenv "TERM" "xterm")
(setenv "TMPDIR" "/tmp")

;; I dont what this means. but looks imp
(setq buffer-file-coding-system 'utf-8-unix)
(setq default-file-name-coding-system 'utf-8-unix)
(setq default-keyboard-coding-system 'utf-8-unix)
(setq default-process-coding-system '(utf-8-unix . utf-8-unix))
(setq default-sendmail-coding-system 'utf-8-unix)
(setq default-terminal-coding-system 'utf-8-unix)

;; Increase message log
(setq message-log-max 5000)

(setq-default
 inhibit-startup-screen t               ; Skip the startup screens
 initial-scratch-message nil
 frame-title-format '(buffer-file-name "%f" "%b") ; I already know this is Emacs
 truncate-lines t                     ; Truncate lines, don't wrap
 paren-mode 'sexp                     ; Highlight parenthesis
 blink-cursor-alist '((t . hollow))   ; Cursor blinks solid and hollow
 disabled-command-function nil  ; Don't second-guess advanced commands
 kill-read-only-ok t            ; Silently copy in read-only buffers
 tab-width 4                    ; Set tab stops
 tab-stop-list '(4 8 12 16 20 24 28 32 36 40 44
                   48 52 56 60 64 68 72 76 80 84)
 indent-tabs-mode nil             ; Use spaces only, no tabs
 page-delimiter "^\\s *\n\\s *"   ; Page delim one or more blank lines
 minibuffer-max-depth nil         ; Mini-buffer settings
 display-time-day-and-date t ; Display the time and date on the mode line
 case-fold-search t          ; Fold case on searches
 )

;;; remove crap offerings when finding something
(setq completion-ignored-extensions
      '(".o" ".elc" "~" ".bin" ".bak" ".obj" ".map" ".a" ".ln" ".mod"))

;; Always end a file with a newline
(setq require-final-newline t)

;; Stop at the end of the file, not just add lines
;;(setq next-line-add-newlines nil)
(setq disabled-command-function nil)

;;; Ubuntu needs this(?)
(setq browse-url-browser-function 'browse-url-firefox)

(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

;;; damn IMPORTANT.
(defalias 'yes-or-no-p 'y-or-n-p)

;;; Dont ask me when a process is alive while I kill a buffer
(setq kill-buffer-query-functions
      (remq 'process-kill-buffer-query-function
            kill-buffer-query-functions))

;;; make executable if shebang is present
(add-hook 'after-save-hook
          'executable-make-buffer-file-executable-if-script-p)

;;;---------------------------------------------------------------------
;;; change behavious of builtins

(defadvice kill-line (around kill-region-if-active activate)
  "kill region if active with C-k"
  (if (and (called-interactively-p) (region-active-p))
      (kill-region (region-beginning) (region-end))
    ad-do-it))

(defadvice yank (after indent-region activate)
  "To make yank content indent automatically."
  (if (member major-mode '(emacs-lisp-mode
                           scheme-mode
                           lisp-mode
                           lisp-interaction-mode
                           c-mode
                           c++-mode
                           objc-mode
                           latex-mode
                           plain-tex-mode))
      (indent-region (region-beginning) (region-end) nil)))

;;; search
(setq search-whitespace-regexp "[ \t\r\n]+")

;;; delete nasty hidden white spaces at the end of lines
(add-hook 'before-save-hook (lambda () (delete-trailing-whitespace)))

;;; buttonize addresses
(add-hook 'find-file-hooks 'goto-address-prog-mode)

;;; init-general.el ends here
